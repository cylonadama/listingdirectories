## Cross-platform directory listing micro-service that runs in a Docker container,

### Prerequisites
- Docker installed on machine
- Java 8
- Apache Maven 3.6.1 

### Build
To build the micro-service run 

```bash
mvn clean install
```

### Building the Docker image
In the projects root directory run the command below:

```bash
docker build -t dirlisting .
```

you can then proceed to check if the image has been created 
```bash
docker image ls
```

Once you have confirmation of this you can then proceed to run the app:

```bash
docker run -p 8080:8080 dirlisting
```
The app should now be available on:  http://localhost:8080/

### NB: 
The micro-service leverages pagination to get the directory listing. Currently this comes in the form of 
2 parameters that we need to pass: page(default is 1) and pagesize(default is 50) 
The pagination strategy might be improved using a HATEOAS implementation.

### Important links and Info 
The API has been documented using Swagger and you can access the documentation on the link below:
http://localhost:8080/swagger-ui.html#/

The Directory Listing endpoint will be under the dir-listing-controller accordion.

All logs are located in the app.log and the config is in resources/logback-spring.xml
this is where the archiving strategy is also configured.

For monitoring I have leveraged spring-boot-actuator. The exposed endpoints can also be seen at:
http://localhost:8080/swagger-ui.html#/ 
under the operation-handler accordion. To do a health check you can do a GET http://localhost:8080/actuator/health

### Docker-Compose 
You can also run this app using docker-compose to do this, from the project root directory you can run:
```bash
docker-compose up
```  
We can also convert the  docker-compose file to a deployment config for kubernetes(k8s)  deployment.  