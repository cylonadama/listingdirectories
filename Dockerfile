
FROM openjdk:8

VOLUME /app

# The application's jar file
ARG JAR_FILE=target/dirlisting-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} dirlisting-0.0.1-SNAPSHOT.jar

EXPOSE 8080 9080

ENTRYPOINT ["java","-jar","dirlisting-0.0.1-SNAPSHOT.jar"]
