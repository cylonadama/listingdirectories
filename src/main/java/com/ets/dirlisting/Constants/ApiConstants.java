package com.ets.dirlisting.Constants;

/**
 * All constants will be placed here.
 */
public class ApiConstants {
    public static final String MESSAGE_FOR_INVALID_PARAMETERS_ERROR = "Invalid Parameters";
    public static final String MESSAGE_FOR_INVALID_BODY_ERROR = "Invalid Method Body. Check JSON Objects";
    public static final String DATE_FORMAT = "dd-MM-yyyy hh:mm:ss";
    public static final String CURRENT_DIR_PATH = "./";
    public static final String INVALID_PATH = "Path does not exist or is not a directory";
    public static final String ERROR_RESOLVING_PATH = "*Error resolving path*";
    public static final String DIRECTORY = "Directory";
    public static final String FILE = "File";
    public static final String DIRECTORY_STREAM_ERROR = "Error creating directory stream";
}
