package com.ets.dirlisting.service;

/**
 *  We are using the International Electrotechnical Commission (IEC)  prefixes (1998).
 */
public enum DataUnit {
        B,
        KiB,
        MiB,
        GiB,
        TiB,
        PiB,

}
