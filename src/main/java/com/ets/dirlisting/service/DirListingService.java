package com.ets.dirlisting.service;

import com.ets.dirlisting.model.DirListing;

import java.util.List;

public interface DirListingService {
    List<DirListing> getDirectoryListing(String absPath, Integer page, Integer pageSize);
}
