package com.ets.dirlisting.service;

import com.ets.dirlisting.Constants.ApiConstants;
import com.ets.dirlisting.Exception.ApiError;
import com.ets.dirlisting.model.DirListing;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.ets.dirlisting.Constants.ApiConstants.*;

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class DirListingServiceImpl implements DirListingService {

    private Logger logger = LoggerFactory.getLogger(DirListingServiceImpl.class);

    /**
     * Return a list of directory contents given the path
     * @param absPath unique absolute path for a given file
     * @param page the page index for pagination
     * @param pageSize the number of items to return per page
     * @return Directory Listing
     */
    @Override
    public List<DirListing> getDirectoryListing(String absPath, Integer page, Integer pageSize) {
        File dir = new File(absPath);
        if (!(dir.exists() && dir.isDirectory())) {
            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ApiConstants.INVALID_PATH);
            logger.error(INVALID_PATH, apiError);
            throw apiError;
        }

        DirectoryStream<Path> directoryStream = getDirectoryStream(dir);
        ArrayList<DirListing> dirListings = new ArrayList<>();

        int i = 0;
        int fromIndex =  (page - 1) * pageSize;
        int toIndex = fromIndex + pageSize;
        for (Path path : directoryStream) {
            if (i >= fromIndex && i < toIndex) {
                File pathFile = path.toFile();
                DataUnit dataUnit = DataUnit.B;
                long size = pathFile.length();

                while (size > 1024) {
                    size /= 1024;
                    dataUnit = DataUnit.values()[dataUnit.ordinal() +  1];
                }

                String fullPath = pathFile.getName();

                String info = getInfo(pathFile);
                DirListing listing = new DirListing(fullPath, String.format("%d %s", size, dataUnit.name()), info);
                dirListings.add(listing);
            } else if (i >= toIndex) {
                break;
            }
            i++;
        }

        return dirListings;
    }

    /**
     * Since we are using external iteration we will use DirectoryStream,
     * If we want to take the advantage of Stream API operations we can use
     * a List at a later stage as an improvement.
     * @param dir supplied dir
     * @return
     */
    private DirectoryStream<Path> getDirectoryStream(File dir) {
        DirectoryStream<Path> directoryStream;
        try {
            directoryStream = Files.newDirectoryStream(dir.toPath());
        } catch (IOException e) {
            logger.error(DIRECTORY_STREAM_ERROR, e);
            throw new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
        return directoryStream;
    }

    /**
     * Determine if item in path is a directory or file.
     * @param pathFile the item we are inspecting
     * @return info can either be directory or file.
     */
    private String getInfo(File pathFile) {
        String info = "";
        if  (pathFile.isDirectory()) {
            info = DIRECTORY;
        } else if (pathFile.isFile()) {
            info  = FILE;
        }
        return info;
    }

}
