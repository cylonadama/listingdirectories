package com.ets.dirlisting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DirListing {
    private String fullPath;
    private String fileSize;
    private String DirInformation;
}
