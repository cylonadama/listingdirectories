package com.ets.dirlisting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DirListingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DirListingApplication.class, args);
	}

}
