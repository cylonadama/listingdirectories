package com.ets.dirlisting.controller;

import com.ets.dirlisting.model.DirListing;
import com.ets.dirlisting.service.DirListingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.ets.dirlisting.Constants.ApiConstants.CURRENT_DIR_PATH;

@RestController
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@RequestMapping("/api/v1")
public class DirListingController {

    /**
     * Injecting service.
     */
    @Autowired
    private final DirListingService listingService;

    /**
     * Given a path return the directory listing information.
     *
     * @param path     the path to the directory to show listing
     * @param page     the page index for pagination support
     * @param pageSize the number of items to return per page
     * @return ResponseEntity object with dirListings and HTTP status
     */
    @GetMapping(value = "/list")
    public ResponseEntity<List<DirListing>> getDirListingByPath(@RequestParam(value = "path", defaultValue = CURRENT_DIR_PATH, required = false) String path,
                                                                @RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
                                                                @RequestParam(value = "pageSize", defaultValue = "50", required = false) Integer pageSize) {

        List<DirListing> dirListings = listingService.getDirectoryListing(path, page, pageSize);

        return new ResponseEntity<>(dirListings, HttpStatus.OK);
    }

}
